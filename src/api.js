// src/api.js
const API_KEY = process.env.REACT_APP_TMDB_API_KEY;

export const fetchTrending = `https://api.themoviedb.org/3/trending/all/week?api_key=${API_KEY}`;
export const fetchTopRated = `https://api.themoviedb.org/3/movie/top_rated?api_key=${API_KEY}`;
// Add more endpoints as needed
