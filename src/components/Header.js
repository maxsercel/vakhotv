// src/components/Header.js
import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

const Header = () => {
  return (
    <header className="header">
      <Link to="/" className="logo">Lelflix</Link>
      <input type="text" placeholder="Search..." className="search-input" />
    </header>
  );
};

export default Header;
