// src/components/Slider.js
import React from 'react';
import './Slider.css';

const Slider = () => {
  const slides = [
    { title: 'Fast And Furious X', img: 'url_to_image' },
    { title: 'Money Heist', img: 'url_to_image' },
    { title: 'Inside Out 2', img: 'url_to_image' },
  ];

  return (
    <div className="slider">
      {slides.map((slide, index) => (
        <div className="slide" key={index} style={{ backgroundImage: `url(${slide.img})` }}>
          <h2>{slide.title}</h2>
        </div>
      ))}
    </div>
  );
};

export default Slider;
