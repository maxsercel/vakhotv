// src/components/Player.js
import React, { useState } from 'react';
import './Player.css';

const Player = ({ tmdbId, isMovie }) => {
  const [server, setServer] = useState(1);
  const [season, setSeason] = useState(1);
  const [episode, setEpisode] = useState(1);

  const getIframeSrc = () => {
    let src = '';
    switch (server) {
      case 1:
        src = isMovie ? `https://vidsrc.to/embed/movie/${tmdbId}` : `https://vidsrc.to/embed/tv/${tmdbId}/${season}/${episode}`;
        break;
      case 2:
        src = isMovie ? `https://moviesapi.club/movie/${tmdbId}` : `https://moviesapi.club/tv/${tmdbId}-${season}-${episode}`;
        break;
      case 3:
        src = isMovie ? `https://multiembed.mov/?video_id=${tmdbId}&tmdb=1` : `https://multiembed.mov/?video_id=${tmdbId}&tmdb=1&s=${season}&e=${episode}`;
        break;
      default:
        src = `https://vidsrc.to/embed/movie/${tmdbId}`;
    }
    return src;
  };

  return (
    <div className="player">
      <iframe src={getIframeSrc()} frameBorder="0" allowFullScreen></iframe>
      <div className="player-controls">
        <div className="server-selector">
          <button onClick={() => setServer(1)}>Server 1</button>
          <button onClick={() => setServer(2)}>Server 2</button>
          <button onClick={() => setServer(3)}>Server 3</button>
        </div>
        {!isMovie && (
          <div className="season-episode-selector">
            <select value={season} onChange={(e) => setSeason(e.target.value)}>
              {/* Populate options dynamically */}
              {[1, 2, 3].map((num) => (
                <option key={num} value={num}>Season {num}</option>
              ))}
            </select>
            <select value={episode} onChange={(e) => setEpisode(e.target.value)}>
              {/* Populate options dynamically */}
              {[1, 2, 3].map((num) => (
                <option key={num} value={num}>Episode {num}</option>
              ))}
            </select>
          </div>
        )}
      </div>
    </div>
  );
};

export default Player;
