// src/pages/MoviePage.js
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import Player from '../components/Player';
import './MoviePage.css';

const MoviePage = () => {
  const { id } = useParams();
  const [movie, setMovie] = useState({});
  const [isMovie, setIsMovie] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const request = await axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.REACT_APP_TMDB_API_KEY}`);
      setMovie(request.data);
      setIsMovie(true);
    };
    fetchData();
  }, [id]);

  return (
    <div className="movie-page">
      <Player tmdbId={id} isMovie={isMovie} />
      <div className="movie-details">
        <img src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`} alt={movie.title} />
        <div className="movie-info">
          <h1>{movie.title}</h1>
          <p>{movie.overview}</p>
          <p>Release Date: {movie.release_date}</p>
          <p>Actors: {/* List actors here */}</p>
        </div>
      </div>
    </div>
  );
};

export default MoviePage;
