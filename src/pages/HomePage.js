// src/pages/HomePage.js
import React from 'react';
import Slider from '../components/Slider';
import MovieList from '../components/MovieList';
import { fetchTrending, fetchTopRated } from '../api';
import './HomePage.css';

const HomePage = () => {
  return (
    <div className="home-page">
      <Slider />
      <MovieList title="Trending" fetchUrl={fetchTrending} />
      <MovieList title="Top Rated" fetchUrl={fetchTopRated} />
    </div>
  );
};

export default HomePage;
